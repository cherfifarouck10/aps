(* ========================================================================== *)
(* == UPMC/master/info/4I506 -- Janvier 2016/2017/2018                     == *)
(* == SU/FSI/master/info/MU4IN503 -- Janvier 2020/2021/2022                == *)
(* == Analyse des programmes et sémantiques                                == *)
(* ========================================================================== *)
(* == hello-APS Syntaxe ML                                                 == *)
(* == Fichier: ast.ml                                                      == *)
(* ==  Arbre de syntaxe abstraite                                          == *)
(* ========================================================================== *)

type typ = 
    ASTBool 
    | ASTInt
    | ASTFunType of types * typ 
and 
    types = 
    ASTType of typ 
    | ASTTypes of typ * types 

type arg = 
    string * typ

type args = 
    ASTArg of arg 
  | ASTArgs of arg * args

type expr =
    ASTNum of int
  | ASTId of string
  | ASTApp of expr * expr list
  | ASTIf of expr * expr * expr 
  | ASTOr of expr * expr 
  | ASTAnd of expr * expr 
  | ASTAbs of args * expr 


type def = 
    ASTConst of string * typ * expr 
    | ASTFun of string * typ * args * expr 
    | ASTFunRec of string * typ * args * expr


type stat =
    ASTEcho of expr

type exprs = 
    ASTExprs of expr  
    | ASTexprs of expr * exprs 
      
type cmd =
    ASTStat of stat
    | ASTCmd of def * cmd