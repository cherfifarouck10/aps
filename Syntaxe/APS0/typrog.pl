g0([
    (true,bool),
    (false,bool),
    (not, [bool, bool]),
    (eq, [int,int,bool]),
    (lt, [int,int,bool]),
    (add, [int,int,int]),
    (sub, [int,int,int]),
    (mul, [int,int,int]),
    (div, [int,int,int])
]).


/* g0(X):-search(X,Y).
search(X, Y) :- member((X, Y), [
    (true,bool),
    (false,bool),
    (not, [bool, bool]),
    (eq, [[int,  int] -> bool]),
    (lt, [[int, int] -> bool]),
    (add, [[int, int] -> int]),
    (sub, [[int, int] -> int]),
    (mul, [[int, int] -> int]),
    (div, [[int, int] -> int])
]),write(Y). */


/* Prog */ 
typeProg(prog(X),void) :- typeCmds(L,X,void), g0(L).

/* Cmds */ 
/* Defs */ 
typeCmds(G,[],void).
typeCmds(G,[stat(X) | Y], void) :- typeStat(G,X,void), typeCmds(G,Y,void).
typeCmds(G,[ def(X) | Y], void) :- typeDef(G,X,NG), typeCmds(G,Y,void).


/* Instruction Echo */ 
/* Stat */ 
/*End */ 
typeStat(G,echo(X),void) :- typeExpr(G,X,int).

/* Def */ 
/* Const */     
typeDef(G,const(X,T,E),[(X,T) | G]) :- typeExpr(G,E,T).


/* Fun */ 
typeDef(G,fun(X,T,[],EXPR), [(EXPR,T) | G]).
typeDef(G,fun(X,T,[(ARG,TYPE) | ARGS], EXPR), NG) :-
                append([(ARG,TYPE)], G, NG),
                typeDef(NG,fun(X,T,ARGS,EXPR),NNG), typeExpr(NNG, EXPR,T).
            

/* FunRec */ 

typeDef(G,funrec(X,T,[],EXPR), [(EXPR,T) | G]).
typeDef(G,funrec(X,T,[(ARG,TYPE) | ARGS], EXPR), NG) :-
                append([(ARG,TYPE)], G, NG),
                typeDef(NG,funrec(X,T,ARGS,EXPR),NNG), typeExpr(NNG, EXPR,T).

/* Num */ 
typeExpr(G,int) :- integer(X).

/* Ident */ 
typeExpr([(X,T) | Y ],id(X),T). 
typeExpr([ _ | Y], id(X), T) :- typeExpr(Y,id(X),T). 

/* Abs  ?- typeExpr([(x,int), (y,int), (x+y,int)], func([id(x),id(y)],id(x+y)),int). */ 

/*typeExpr(G, func([(X, TI)], EXPR),arrow([TI], T) :-  typeExpr(G,EXPR,T).
typeExpr(G,func([(X, TI) | ARGS ],EXPR),arrow([TI|TS],T)) :- typeExpr([(X,TI) | G], func(ARGS,EXPR),arrow(TS,T)).*/

/* envExt(G, BS, G2). */

typeExpr(G, func(ARGS, EXPR),arrow(TS, T)):- envExt(G, ARGS, G2), typeExpr(G2, EXPR, T),typeofarg(ARGS, TS).
envExt(G, [], G).
envExt(G, [B | BS] , [B | G2]):- envExt(G, BS, G2).

typeofarg([], []).
typeofarg([( _, T) | BS] , [T|TS]):- typeofarg(BS, TS).

/*typeExpr(G, func(ARG, EXPR),TF)*/

/* App */ 
/* ?- typeExpr([(x,int)], app(func([id(x)],id(x))),int). */

/* Application de fonction */ 

/*typeExpr(G, app(func([],EXPR)), TF).
typeExpr(G, app(func([ARG | ARGS],EXPR)), TF) :- 
                    typeExpr(G,ARG,T), 
                    typeExpr(G,EXPR,TF),
                    typeExpr(G,app(func(ARGS,EXPR)),TF).*/


typeExpr(G, app(E, ES), T):- typeExpr(G, E, arrow(TS,T)),typeExprList(G,ES, TS).
typeExprList(G, [], []).
typeExprList(G, [E | Es], [T | TS]):- typeExpr(G, E ,T),
                                    typeExprList(G, ES, TS).
/* And */ 
/* Debug le false true */ 
typeExpr(G,and(E1,E2), bool) :- typeExpr(X,E1,bool), g0(X),
                                typeExpr(Y,E2,bool), g0(Y).
/* Or */ 
typeExpr(G,or(E1,E2), bool) :- typeExpr(X,E1,bool), g0(X),
                                typeExpr(Y,E2,bool), g0(Y).

/* If */ 
typeExpr(G,if(E1,E2,E3),T) :- typeExpr(G,E1,bool), typeExpr(G,E2,T), typeExpr(G,E3,T). 

/* Echo */

echo(E) :- write(E).

main_stdin :-
    read(user_input,T), 
    typeProg(T,R).
