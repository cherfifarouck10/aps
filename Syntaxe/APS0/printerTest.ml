open Ast


let rec print_type t = 
  match t with
  ASTBool -> Printf.printf"bool "
  | ASTInt -> Printf.printf"int " 
  | ASTFunType(typelist, typ) -> (
    Printf.printf" (";
    print_FunType typelist ; 
    print_type typ;
    Printf.printf")"
  )

and print_FunType ft = 
  match ft with 
    ASTType(t) -> (
      print_type t ; 
      Printf.printf" -> "
    )
    
    | ASTTypes(t,ts) -> (
      print_type t;
      print_FunType ts
    )
  

let rec print_args a = 
  match a with 
    ASTArg(a1) -> (
      match a1 with 
       (s,t) -> (
        Printf.printf"%s : " s;
        print_type t
       )
    )
    | ASTArgs(a1,al) -> (
      match a1 with 
       (s,t) -> (
        Printf.printf"%s" s;
        print_type t
       );
       print_args al
    )

let rec print_expr e =
  match e with
      ASTNum n -> Printf.printf"%d" n
    | ASTId x -> Printf.printf"%s" x
    | ASTApp(e, es) -> (
	Printf.printf"(";
	print_expr e;
	Printf.printf" ";
	print_exprs es;
	Printf.printf")"
      )
    | ASTIf(b,c,a) -> (
    Printf.printf"(if ";
    print_expr b; 
    print_expr c;
    print_expr a;
    Printf.printf")"
    )
    | ASTAnd(e1,e2) -> (
      Printf.printf"( and ";
      print_expr e1;
      print_expr e2;
      Printf.printf" )"

    )
    | ASTOr(e1,e2) -> (
      Printf.printf"( or ";
      print_expr e1;
      print_expr e2;
      Printf.printf" )"
    )
    | ASTAbs(args,e1) -> (
      Printf.printf"[ ";
      print_args args;
      Printf.printf" ] ";
      print_expr e1
    )

and print_exprs es =
  match es with
      [] -> ()
    | [e] -> print_expr e
    | e::es -> (
	print_expr e;
	print_char ',';
	print_exprs es
      )

let print_stat s =
  match s with
      ASTEcho e -> (
	Printf.printf("ECHO ");
	print_expr(e)
      )



let print_def d = 
  match d with 
    ASTConst(i,t,e) -> (
      Printf.printf"CONST %s " i ; print_type t; print_expr e; Printf.printf" ; ")
    |  ASTFunRec(s, t, a, e) -> (
      Printf.printf"FUN REC %s" s;
      print_type t;
      Printf.printf" [ ";
      print_args a;
      Printf.printf" ] ";
      print_expr e;
      Printf.printf"; "
    )
    | ASTFun(s,t,a,e) -> (
      Printf.printf"FUN %s " s;
      print_type t;
      Printf.printf" [ ";
      print_args a;
      Printf.printf" ] ";
      print_expr e;
      Printf.printf"; "
      )

let rec print_cmd c =
  match c with
      ASTStat s -> print_stat s 
      | ASTCmd(d,cmd) -> print_def d ; print_cmd cmd 
	
let rec print_cmds cs =
  match cs with
      c::[] -> print_cmd c
    | _ -> failwith "not yet implemented"
	
let print_prog p =
  Printf.printf"[";
  print_cmds [p];
  Printf.printf"]"
;;


let fname = Sys.argv.(1) in
let ic = open_in fname in
  try
    let lexbuf = Lexing.from_channel ic in
    let p = Parser.prog Lexer.token lexbuf in
      print_prog p;
      print_string "\n"
  with Lexer.Eof ->
    exit 0