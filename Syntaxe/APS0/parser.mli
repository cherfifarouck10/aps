type token =
  | NUM of (int)
  | IDENT of (string)
  | LPAR
  | RPAR
  | LBRA
  | RBRA
  | PVIRG
  | VIRG
  | DPOIN
  | ETOI
  | FLEC
  | ECHO
  | CONST
  | FUN
  | REC
  | IF
  | AND
  | OR
  | BOOL
  | INT

val prog :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Ast.cmd
