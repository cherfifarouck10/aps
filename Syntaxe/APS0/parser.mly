%{
(* ========================================================================== *)
(* == UPMC/master/info/4I506 -- Janvier 2016/2017                          == *)
(* == SU/FSI/master/info/MU4IN503 -- Janvier 2020/2021/2022                == *)
(* == Analyse des programmes et sémantiques                                == *)
(* ========================================================================== *)
(* == hello-APS Syntaxe ML                                                 == *)
(* == Fichier: parser.mly                                                  == *)
(* == Analyse syntaxique                                                   == *)
(* ========================================================================== *)

open Ast

%}
  
%token <int> NUM
%token <string> IDENT
%token LPAR RPAR 
%token LBRA RBRA
%token PVIRG VIRG DPOIN PVIRG ETOI FLEC
%token ECHO CONST FUN REC
%token IF AND OR BOOL INT


%type <Ast.expr> expr
%type <Ast.expr list> exprs
%type <Ast.cmd> cmds
%type <Ast.cmd> prog


%start prog

%%
prog: LBRA cmds RBRA    { $2 }
;

cmds:
  stat                  { ASTStat($1) }
| def PVIRG cmds        { ASTCmd($1,$3)}
;

def: 
  CONST IDENT typ expr { ASTConst($2,$3,$4) }
| FUN IDENT typ LBRA args RBRA expr {ASTFun($2,$3,$5,$7)}
| FUN REC IDENT typ LBRA args RBRA expr {ASTFunRec($3,$4,$6,$8)}

typ: 
  BOOL {ASTBool}
| INT {ASTInt}
| LPAR types FLEC typ RPAR {ASTFunType($2,$4)}

types :
  typ {ASTType($1)}
| typ ETOI types {ASTTypes($1,$3)}
  

args:
  arg { ASTArg($1) }
| arg PVIRG args {ASTArgs($1, $3)}

arg:
  IDENT DPOIN typ {($1, $3)}


stat:
  ECHO expr             { ASTEcho($2) }
;

expr:
  NUM                   { ASTNum($1) }
| IDENT                 { ASTId($1) }
| LPAR IF expr expr expr RPAR  { ASTIf($3,$4,$5) }   
| LPAR AND expr expr RPAR { ASTAnd($3,$4) }
| LPAR OR expr expr RPAR { ASTOr($3,$4) }
| LPAR expr exprs RPAR  { ASTApp($2, $3) }
| LBRA args RBRA expr {ASTAbs($2, $4)}
;

exprs :
  expr       { [$1] }
| expr exprs { $1::$2 }
;
